////////////////////////////////////////////////
// Link: https://www.acmicpc.net/problem/6359
// Writer: keicoon15@gmail.com
////////////////////////////////////////////////
#include <stdio.h>

int main()
{
	int T;
	scanf("%d", &T);
	while (T-- > 0)
	{
		int n;
		scanf("%d", &n);
		bool isOpened[101] = {};

		for (int i = 1;i <= n; ++i)
			for (int j = i; j <= n && (((j % i == 0) && (isOpened[j] = !isOpened[j])) || true); ++j);

		int c = 0;
		for (int j = 1;j <= n && ((isOpened[j] && ++c) || true); ++j);

		printf("%d\n", c);
	}
}