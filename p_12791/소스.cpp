////////////////////////////////////////////////
// Link: https://www.acmicpc.net/problem/12790
// Writer: keicoon15@gmail.com
////////////////////////////////////////////////
#include <stdio.h>
#define ATTACKPOWER(HP, MP, ATTACK, DEFENCE) (1 * (HP < 1 ? 1 : HP) + 5 * (MP < 1 ? 1 : MP) + 2 * (ATTACK < 0 ? 0 : ATTACK) + 2 * DEFENCE)

int main()
{
	int T;
	scanf("%d", &T);

	while (T-- > 0)
	{
		int cHp, cMp, cAttack, cDefence, eHp, eMp, eAttack, eDefence;
		scanf("%d %d %d %d %d %d %d %d",
			&cHp, &cMp, &cAttack, &cDefence, &eHp, &eMp, &eAttack, &eDefence);

		printf("%d\n", ATTACKPOWER((cHp + eHp), (cMp + eMp), (cAttack + eAttack), (cDefence + eDefence)));
	}
	return 0;
}